package com.therishka.zimadtest.Models;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rishad Mustafaev
 */

public class ApiResult {

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private List<SingleAnimal> resultData;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @NonNull
    public List<SingleAnimal> getAnimals() {
        return resultData != null ? resultData : new ArrayList<SingleAnimal>();
    }

    public void setResultData(@Nullable List<SingleAnimal> resultData) {
        this.resultData = resultData;
    }
}
