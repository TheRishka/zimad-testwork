package com.therishka.zimadtest.Models;

import com.google.gson.annotations.SerializedName;

/**
 * @author Rishad Mustafaev
 */

public class SingleAnimal {
    @SerializedName("url")
    String imageLink;

    @SerializedName("title")
    String imageTitle;

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }
}
