package com.therishka.zimadtest;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;

/**
 * @author Rishad Mustafaev.
 */
public abstract class StateActivity extends AppCompatActivity {

    private static final String KEY_SAVED_STATE_LAST_STATE = "LAST_STATE";

    private View pErrorView;
    private View pLoadingView;
    private View pSuccessView;

    protected ActivityState pState = ActivityState.ERROR;

    enum ActivityState {
        ERROR("error"), LOADING("loading"), SUCCESS("success");

        private final String mName;

        ActivityState(@NonNull String name) {
            mName = name;
        }

        public static ActivityState getState(@NonNull String state) {
            if (TextUtils.isEmpty(state)) {
                return ERROR;
            }
            for (ActivityState activityState : values()) {
                if (TextUtils.equals(state.toLowerCase(), activityState.mName.toLowerCase())) {
                    return activityState;
                }
            }
            return ERROR;
        }

        @Override
        public String toString() {
            return this.name().toLowerCase();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(provideLayoutId());
        initViews();
        pErrorView = provideErrorView();
        pLoadingView = provideLoadingView();
        pSuccessView = provideSuccessView();
        if (savedInstanceState != null) {
            ActivityState state = ActivityState.getState(savedInstanceState.getString(KEY_SAVED_STATE_LAST_STATE, ""));
            setState(state);
        }
    }

    @LayoutRes
    protected abstract int provideLayoutId();

    protected abstract void initViews();

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_SAVED_STATE_LAST_STATE, pState.toString());
    }

    protected void setState(@NonNull ActivityState activityState) {
        switch (activityState) {
            case LOADING:
                setLoading();
                break;
            case ERROR:
                setError();
                break;
            case SUCCESS:
                setSuccess();
                break;
        }
    }

    protected void setError() {
        pState = ActivityState.ERROR;
        pLoadingView.setVisibility(View.GONE);
        pSuccessView.setVisibility(View.INVISIBLE);
        pErrorView.setVisibility(View.VISIBLE);
    }

    protected void setLoading() {
        pState = ActivityState.LOADING;
        pErrorView.setVisibility(View.GONE);
        pSuccessView.setVisibility(View.INVISIBLE);
        pLoadingView.setVisibility(View.VISIBLE);
    }

    protected void setSuccess() {
        pState = ActivityState.SUCCESS;
        pErrorView.setVisibility(View.GONE);
        pLoadingView.setVisibility(View.GONE);
        pSuccessView.setVisibility(View.VISIBLE);
    }

    protected abstract View provideErrorView();

    protected abstract View provideLoadingView();

    protected abstract View provideSuccessView();
}
