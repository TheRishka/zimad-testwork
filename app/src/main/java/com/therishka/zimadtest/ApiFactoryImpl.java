package com.therishka.zimadtest;

import android.support.annotation.NonNull;

import com.therishka.zimadtest.Models.ApiResult;

import java.io.IOException;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Rishad Mustafaev
 */

public class ApiFactoryImpl implements ApiFactory {

    @Override
    public Response<ApiResult> provideApiResults(@NonNull QueryVariant variant) throws IOException {
        return getApi().getAnimal(variant.toString()).execute();
    }

    private TestworkAPI getApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(TestworkAPI.class);
    }
}
