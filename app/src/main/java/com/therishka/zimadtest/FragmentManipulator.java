package com.therishka.zimadtest;

import android.support.annotation.NonNull;

import com.therishka.zimadtest.Models.SingleAnimal;

/**
 * @author Rishad Mustafaev
 */

interface FragmentManipulator {
    void changeToolbarTitle(@NonNull String title);
    void clickedOnElement(@NonNull SingleAnimal animal);
}
