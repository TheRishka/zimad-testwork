package com.therishka.zimadtest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.therishka.zimadtest.Models.SingleAnimal;

public class AnimalActivity extends StateActivity {

    private static final String BUNDLE_KEY_IMAGE_LINK = "image_link";
    private static final String BUNDLE_KEY_TEXT = "text";

    public static void startMe(@NonNull Activity activity,
                               @NonNull SingleAnimal animal) {
        Intent intent = new Intent(activity, AnimalActivity.class);
        intent.putExtra(BUNDLE_KEY_IMAGE_LINK, animal.getImageLink());
        intent.putExtra(BUNDLE_KEY_TEXT, animal.getImageTitle());

        activity.startActivity(intent);
    }

    private ProgressBar mLoading;
    private ScrollView mContent;
    private LinearLayout mError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView animalText = (TextView) findViewById(R.id.ac_animal_text);
        ImageView animalImage = (ImageView) findViewById(R.id.ac_animal_image);

        Bundle bundle = getIntent().getExtras();
        String text = bundle.getString(BUNDLE_KEY_TEXT, "");
        animalText.setText(text);
        setLoading();
        Glide.with(this).
                load(bundle.getString(BUNDLE_KEY_IMAGE_LINK, "")).
                asGif().
                crossFade().
                override(300, 300).
                listener(new RequestListener<String, GifDrawable>() {
                    @Override
                    public boolean onException(Exception e,
                                               String model,
                                               Target<GifDrawable> target,
                                               boolean isFirstResource) {
                        setError();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GifDrawable resource, String model,
                                                   Target<GifDrawable> target,
                                                   boolean isFromMemoryCache,
                                                   boolean isFirstResource) {
                        setSuccess();
                        return false;
                    }
                }).
                into(animalImage);
    }

    @LayoutRes
    @Override
    protected int provideLayoutId() {
        return R.layout.ac_animal;
    }

    @Override
    protected void initViews() {
        mLoading = (ProgressBar) findViewById(R.id.ac_animal_progress);
        mContent = (ScrollView) findViewById(R.id.ac_animal_content);
        mError = (LinearLayout) findViewById(R.id.error_layout);
    }

    @Override
    protected View provideErrorView() {
        return mError;
    }

    @Override
    protected View provideLoadingView() {
        return mLoading;
    }

    @Override
    protected View provideSuccessView() {
        return mContent;
    }
}
