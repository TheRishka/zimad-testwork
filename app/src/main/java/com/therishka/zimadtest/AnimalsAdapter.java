package com.therishka.zimadtest;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.therishka.zimadtest.Models.SingleAnimal;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rishad Mustafaev.
 */
class AnimalsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SingleAnimal> mAnimalList = new ArrayList<>();
    private AnimalClickListener mListener;

    AnimalsAdapter(@NonNull AnimalClickListener listener) {
        mListener = listener;
    }

    void setAnimalList(@NonNull List<SingleAnimal> animals) {
        mAnimalList = animals;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_element, parent, false);
        return new AnimalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((AnimalViewHolder) holder).bind(mAnimalList.get(position), mListener);
    }

    @Override
    public int getItemCount() {
        return mAnimalList.size();
    }

    private static class AnimalViewHolder extends RecyclerView.ViewHolder {

        ImageView mImage;
        TextView mText;

        AnimalViewHolder(View itemView) {
            super(itemView);
            mImage = (ImageView) itemView.findViewById(R.id.list_element_image);
            mText = (TextView) itemView.findViewById(R.id.list_element_text);
        }

        void bind(@NonNull final SingleAnimal animal,
                  @NonNull final AnimalClickListener animalClickListener) {
            Glide.with(itemView.getContext()).load(animal.getImageLink())
                    .into(mImage);
            mText.setText(animal.getImageTitle());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    animalClickListener.clickedOnAnimal(animal);
                }
            });
        }
    }

    interface AnimalClickListener {
        void clickedOnAnimal(@NonNull SingleAnimal animal);
    }
}
