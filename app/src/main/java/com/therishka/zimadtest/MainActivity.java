package com.therishka.zimadtest;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.therishka.zimadtest.Models.ApiResult;
import com.therishka.zimadtest.Models.SingleAnimal;

import java.util.Map;

public class MainActivity extends StateActivity implements FragmentManipulator,
        TabLayout.OnTabSelectedListener,
        LoaderManager.LoaderCallbacks<Map<ApiFactory.QueryVariant, ApiResult>>,
        View.OnClickListener {


    public static final String CATS_FRAG_TAG = "kittens_fragment";
    public static final String DOGS_FRAG_TAG = "puppies_fragment";

    private static final String KEY_SAVED_STATE_SELECTED_TAB = "SELECTED_TAB";
    private static final String KEY_SAVED_STATE_TOOLBAR_TITLE = "TOOLBAR_TITLE";

    Toolbar mToolbar;
    TabLayout mTabLayout;
    LinearLayout mErrorLayout;
    ProgressBar mLoading;
    FrameLayout mMainContent;
    Loader<Map<ApiFactory.QueryVariant, ApiResult>> mLoader;
    private ActivityState mState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoader = getLoaderManager().initLoader(0, null, this);
        initToolbar();
        mTabLayout.addOnTabSelectedListener(this);
        if (savedInstanceState == null) {
            initFragments();
            changeTab(ApiFactory.QueryVariant.CAT);
            setLoading();
            mLoader.forceLoad();
        } else {
            int tabPosition = savedInstanceState.getInt(KEY_SAVED_STATE_SELECTED_TAB, 0);
            TabLayout.Tab tab = mTabLayout.getTabAt(tabPosition);
            if (tab != null) {
                tab.select();
            }
            changeToolbarTitle(savedInstanceState.getString(KEY_SAVED_STATE_TOOLBAR_TITLE, ""));
        }
    }

    @LayoutRes
    @Override
    protected int provideLayoutId() {
        return R.layout.ac_main;
    }

    @Override
    protected void initViews() {
        mErrorLayout = (LinearLayout) findViewById(R.id.error_layout);
        mLoading = (ProgressBar) findViewById(R.id.ac_main_loading);
        mTabLayout = (TabLayout) findViewById(R.id.ac_main_tablayout);
        mMainContent = (FrameLayout) findViewById(R.id.ac_main_container);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTabLayout.addOnTabSelectedListener(this);
        mErrorLayout.setOnClickListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mTabLayout.removeOnTabSelectedListener(this);
        mErrorLayout.setOnClickListener(null);
    }

    @Override
    protected View provideErrorView() {
        return mErrorLayout;
    }

    @Override
    protected View provideLoadingView() {
        return mLoading;
    }

    @Override
    protected View provideSuccessView() {
        return mMainContent;
    }

    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.ac_main_toolbar);
        setSupportActionBar(mToolbar);
    }

    private void initFragments() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.ac_main_container,
                PageFragment.newInstance(ApiFactory.QueryVariant.CAT),
                CATS_FRAG_TAG);
        transaction.add(R.id.ac_main_container,
                PageFragment.newInstance(ApiFactory.QueryVariant.DOG),
                DOGS_FRAG_TAG);
        transaction.commit();
    }

    @Nullable
    private PageFragment getParticularPageFragment(@NonNull @FragmentTags String tag) {
        Fragment fragment = getFragmentManager().findFragmentByTag(tag);
        if (fragment == null) {
            getFragmentManager().beginTransaction().add(
                    R.id.ac_main_container,
                    PageFragment.newInstance(tag.equals(CATS_FRAG_TAG) ?
                            ApiFactory.QueryVariant.CAT :
                            ApiFactory.QueryVariant.DOG),
                    tag).commit();
            getFragmentManager().executePendingTransactions();
            fragment = getFragmentManager().findFragmentByTag(tag);
        }
        return (PageFragment) fragment;
    }

    private void changeTab(ApiFactory.QueryVariant queryVariant) {
        Fragment fragmentToShow;
        Fragment fragmentToHide;
        switch (queryVariant) {
            case CAT:
                fragmentToShow = getParticularPageFragment(CATS_FRAG_TAG);
                fragmentToHide = getParticularPageFragment(DOGS_FRAG_TAG);
                break;
            case DOG:
                fragmentToShow = getParticularPageFragment(DOGS_FRAG_TAG);
                fragmentToHide = getParticularPageFragment(CATS_FRAG_TAG);
                break;
            default:
                fragmentToShow = getParticularPageFragment(DOGS_FRAG_TAG);
                fragmentToHide = getParticularPageFragment(CATS_FRAG_TAG);
                break;
        }
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.hide(fragmentToHide).show(fragmentToShow).commit();
        getFragmentManager().executePendingTransactions();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.error_layout) {
            mLoader = getLoaderManager().restartLoader(0, null, this);
            mLoader.forceLoad();
            setLoading();
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        ApiFactory.QueryVariant query = tab.getPosition() == 0 ? ApiFactory.QueryVariant.CAT : ApiFactory.QueryVariant.DOG;
        changeTab(query);
    }

    @Override
    public void onLoadFinished(Loader<Map<ApiFactory.QueryVariant, ApiResult>> loader, Map<ApiFactory.QueryVariant, ApiResult> data) {
        if (data == null) {
            setError();
            return;
        }
        for (ApiFactory.QueryVariant query : data.keySet()) {
            PageFragment frag = getParticularPageFragment(getFragmentTagFromQuery(query));
            if (frag != null) {
                frag.updateData(data.get(query));
            }
        }
        setSuccess();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_SAVED_STATE_SELECTED_TAB, mTabLayout.getSelectedTabPosition());
        // god bless kotlin for null-safety
        CharSequence title = getSupportActionBar() != null ? getSupportActionBar().getTitle() : "";
        outState.putString(KEY_SAVED_STATE_TOOLBAR_TITLE, title != null ? title.toString() : "");
    }

    @FragmentTags
    @NonNull
    private String getFragmentTagFromQuery(@NonNull ApiFactory.QueryVariant query) {
        String result;
        switch (query) {
            case CAT:
                result = CATS_FRAG_TAG;
                break;
            case DOG:
                result = DOGS_FRAG_TAG;
                break;
            default:
                result = CATS_FRAG_TAG;
                break;
        }
        return result;
    }

    @Override
    public void changeToolbarTitle(@NonNull String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public void clickedOnElement(@NonNull SingleAnimal animal) {
        AnimalActivity.startMe(this, animal);
    }

    @Override
    public Loader<Map<ApiFactory.QueryVariant, ApiResult>> onCreateLoader(int id, Bundle args) {
        return new AnimalsLoader(this);
    }

    @Override
    public void onLoaderReset(Loader<Map<ApiFactory.QueryVariant, ApiResult>> loader) {
        // do nothing
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        // ignore this
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        // ignore this
    }

    @StringDef({CATS_FRAG_TAG, DOGS_FRAG_TAG})
    private @interface FragmentTags {
    }
}
