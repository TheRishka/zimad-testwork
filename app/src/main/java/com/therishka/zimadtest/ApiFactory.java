package com.therishka.zimadtest;

import android.support.annotation.NonNull;

import com.therishka.zimadtest.Models.ApiResult;

import java.io.IOException;

import retrofit2.Response;

/**
 * @author Rishad Mustafaev
 */

public interface ApiFactory {

    enum QueryVariant {
        CAT,
        DOG;

        @Override
        public String toString() {
            return this.name().toLowerCase();
        }
    }

    Response<ApiResult> provideApiResults(@NonNull QueryVariant variant) throws IOException;
}
