package com.therishka.zimadtest;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.support.annotation.Nullable;

import com.therishka.zimadtest.Models.ApiResult;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

/**
 * @author Rishad Mustafaev
 */

class AnimalsLoader extends AsyncTaskLoader<Map<ApiFactory.QueryVariant, ApiResult>> {

    private Map<ApiFactory.QueryVariant, ApiResult> mApiResults = new HashMap<>();

    AnimalsLoader(Context context) {
        super(context);
    }

    @Override
    public Map<ApiFactory.QueryVariant, ApiResult> loadInBackground() {
        ApiFactory factory = new ApiFactoryImpl();
        try {
            Response<ApiResult> catResponse = factory.provideApiResults(ApiFactory.QueryVariant.CAT);
            Response<ApiResult> dogResponse = factory.provideApiResults(ApiFactory.QueryVariant.DOG);
            if (detectError(catResponse) || detectError(dogResponse)) {
                return null;
            }
            if (catResponse.code() == 200) {
                mApiResults.put(ApiFactory.QueryVariant.CAT, catResponse.body());
            }
            if (dogResponse.code() == 200) {
                mApiResults.put(ApiFactory.QueryVariant.DOG, dogResponse.body());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mApiResults;
    }

    private boolean detectError(@Nullable Response<ApiResult> response) {
        return response == null || response.body() == null || response.errorBody() != null;
    }
}
