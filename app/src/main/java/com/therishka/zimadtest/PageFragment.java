package com.therishka.zimadtest;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.therishka.zimadtest.Models.ApiResult;
import com.therishka.zimadtest.Models.SingleAnimal;

/**
 * @author Rishad Mustafaev
 */

public class PageFragment extends Fragment implements AnimalsAdapter.AnimalClickListener {

    public static final String BUNDLE_KEY_QUERY = "query";

    private String mTitle = "";

    public static PageFragment newInstance(@NonNull ApiFactory.QueryVariant query) {
        PageFragment fragment = new PageFragment();
        Bundle args = new Bundle();
        args.putString(BUNDLE_KEY_QUERY, query.toString());
        fragment.setArguments(args);
        return fragment;
    }

    private AnimalsAdapter mAnimalsAdapter;
    private FragmentManipulator mFragmentManipulator;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setFragmentManipulator(context);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setFragmentManipulator(activity);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden && mFragmentManipulator != null) {
            mFragmentManipulator.changeToolbarTitle(mTitle);
        }
    }

    private void setFragmentManipulator(@NonNull Context context) {
        if (context instanceof FragmentManipulator) {
            mFragmentManipulator = ((FragmentManipulator) context);
        } else {
            throw new IllegalStateException("Activity must implement FragmentManipulator biatch");
        }
    }

    public void updateData(@NonNull ApiResult result) {
        if (mAnimalsAdapter != null) {
            mAnimalsAdapter.setAnimalList(result.getAnimals());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fmt_tab, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        RecyclerView animalsList = (RecyclerView) view.findViewById(R.id.fmt_tab_list);
        mTitle = getArguments() != null ? getArguments().getString(BUNDLE_KEY_QUERY, "") : "";
        mAnimalsAdapter = new AnimalsAdapter(this);
        animalsList.setLayoutManager(new LinearLayoutManager(getActivity()));
        animalsList.setAdapter(mAnimalsAdapter);
    }

    @Override
    public void clickedOnAnimal(@NonNull SingleAnimal animal) {
        if(mFragmentManipulator != null){
            mFragmentManipulator.clickedOnElement(animal);
        }
    }
}
