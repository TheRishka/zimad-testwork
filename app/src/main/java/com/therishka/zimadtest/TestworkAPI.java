package com.therishka.zimadtest;

import com.therishka.zimadtest.Models.ApiResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author Rishad Mustafaev
 */

public interface TestworkAPI {

    @GET("api.php")
    Call<ApiResult> getAnimal(@Query("query") String animal);

}
